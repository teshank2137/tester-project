import React from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => {
  return (
    <nav className="navbar navbar-dark  justify-content-center" style={{backgroundColor : 'blue'}}>
        <Link className="nav-item nav-link px-3" to="/" style={{color:"white"}}> Home </Link>
        <Link className="nav-item nav-link px-3" to="/vulnerability/exploitable" style={{color:"white"}}> Vulnerability </Link>
        <Link className="nav-item nav-link px-3" to="/assesment/approved" style={{color:"white"}}> Assessment </Link>
    </nav>
  )
}

export default Navbar;


