import React from 'react'
import { Link } from 'react-router-dom'

const Navbar3 = () => {
  return (
    <div>
        <nav className='nav nav-tabs border-dark'>
            <Link className='nav-item nav-link px-3 mt-3' to="/vulnerability/exploitable" style={{color:"black"}}>Exploitable</Link>
            <Link className='nav-item nav-link px-3 mt-3' to="/vulnerability/suspected" style={{color:"black"}}>Suspected</Link>
        </nav>
    </div>
  )
}

export default Navbar3