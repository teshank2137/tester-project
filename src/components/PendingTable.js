import React, { useState, useEffect } from 'react'
import Axios from 'axios'

const PendingTable = () => {

  const [posts, setPosts] = useState({obj: []});

  useEffect(() => {
    const fetchPostList = async () => {
      const {data} = await Axios("https://jsonplaceholder.typicode.com/users")

      setPosts({obj: data})
    }
    fetchPostList()
  }, [setPosts])

  return (
    <div>
        <table className="table table-borderless">
        <thead style={{backgroundColor : '#E6E6FA', borderRadius : '3px'}}>
            <tr>
            <th scope="col">Stage</th>
            <th scope="col">Codename</th>
            <th scope="col">Name</th>
            <th scope="col">Type</th>
            <th scope="col">Creator</th>
            <th scope="col">Created</th>
            <th scope="col">Requested</th>
            </tr>
        </thead>
        <tbody>
          {
            posts.obj && posts.obj.map((item) => (
              <tr key={item.id}>
              <th scope="row">{item.id}</th>
              <td>{item.name}</td>
              <td>{item.name}</td>
              <td>{item.address.city}</td>
              <td>{item.address.street}</td>
              <td>{item.company.name}</td>
              <td>{item.website}</td>
              </tr>
            ))
          }
        </tbody>
        </table>
    </div>
  )
}

export default PendingTable