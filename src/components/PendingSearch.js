import React from 'react'
import PendingTable from './PendingTable'

const PendingSearch = () => {
  return (
      <>
        <div className='p-3 mt-4 border' style={{backgroundColor : '#EDEDFD', borderRadius : '5px'}}>
            <form className="form-inline active-cyan-4 ">
                <input className="form-control form-control-sm  " type="text" placeholder="Search by name or codename" aria-label="Search"/>
                <input className="form-control form-control-sm  mt-2" type="text" placeholder="Filter by stage" aria-label="Search"/>
                <input className="form-control form-control-sm  mt-2" type="text" placeholder="Filter by type" aria-label="Search"/>
                <input className="form-control form-control-sm  mt-2" type="text" placeholder="Filter by added" aria-label="Search"/>
            </form>
        </div>
        <div className='mt-4'>
            <PendingTable></PendingTable>
        </div>
    </>
  )
}

export default PendingSearch