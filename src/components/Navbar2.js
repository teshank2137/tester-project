import React from 'react'
import { Link } from 'react-router-dom'

const Navbar2 = () => {
  return (
    <div>
        <nav className='nav nav-tabs border-dark'>
            <Link className="nav-item nav-link px-3 mt-3" to="/assesment/approved" style={{color:"black"}}> Approved </Link>
            <Link className="nav-item nav-link px-3 mt-3" to="/assesment/pending" style={{color:"black"}}> Pending </Link>
        </nav>
    </div>
  )
}

export default Navbar2