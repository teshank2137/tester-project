import React from "react";
import { Link } from "react-router-dom";


const Approved = () => {
  return (
    <div className="py-md-3">
      <div className="d-flex justify-content-between align-items-center">
        <div>
          <h3>Approved</h3>
          <p>
            All assesment that have been approved and are either scheduled to
            run, currently active or completed
          </p>
        </div>
        <Link to='/assesment/pending/form'><button className="btn btn-primary">Create Assessment</button></Link>
      </div>
      <div>
      </div>
    </div>
  );
};

export default Approved;
