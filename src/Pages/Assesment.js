import React from 'react'
import Navbar2 from '../components/Navbar2';
import { Outlet } from 'react-router-dom'

const Assesment = () => {
  return (
    <div className='container'>
        <Navbar2></Navbar2>
        
        <Outlet/>
    </div>
  )
}

export default Assesment