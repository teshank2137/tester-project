import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from 'axios'

const Form = () => {
  let navigate = useNavigate();

  const url =""
  const [data, setData] = useState({
    codename: "",
    name: "",
    group: "",
    description: "",
    tags: ""
  });

  const submit = (e) => {
    e.preventDefault();
    axios.post(url,{
      codename: data.codename,
      name: data.name,
      group: data.group,
      description: data.description,
      tags: data.tags
    })
    .then(res => {
      console.log(res.data)
    })
  }

  const handle = (e) => {
    const newdata = {...data}
    newdata[e.target.id] = e.target.value
    setData(newdata)
    console.log(newdata)
  }

  return (
    <div className="card mb-3 mt-3 border">
      <div className="row g-0">
        <div className="col-md-8">
          <div className="card-body">
            <div>
              <h5 className="py-md-1 text-primary">General Information</h5>
              <form onSubmit={(e)=> submit(e)}>
                <div className="mt-1">
                  <h6><strong>Assessment Codename</strong><span className="text-danger">*</span></h6>
                  <select
                    className="form-select"
                    aria-label="Default select example"
                    onChange={(e)=>handle(e)}
                    id="codename"
                    value={data.codename}
                    required
                    >
                    <option>Select a Codename</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </select>
                </div>
                <div className="form-group mt-3">
                  <h6><strong>Assessment Name</strong><span className="text-danger">*</span></h6>
                  <input
                    onChange={(e)=>handle(e)}
                    id="name"
                    value={data.name}
                    type="text"
                    className="form-control"
                    placeholder="Assessment Name"
                    required
                  />
                </div>
                <div className="mt-3">
                  <h6><strong>Assessment Group</strong><span className="text-danger">*</span></h6>
                  <select
                    onChange={(e)=>handle(e)}
                    id="group"
                    value={data.group}
                    className="form-select"
                    required
                  >
                    <option>Select an Assessment Group</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </select>
                </div>
                <div className="mt-3 form-group">
                  <h6><strong>Assessment Discription</strong>{""} <small className="text-muted">[optional]</small></h6>
                  <textarea
                    onChange={(e)=>handle(e)}
                    id="description"
                    value={data.description}
                    type="text"
                    className="form-control"
                    rows="5"
                  ></textarea>
                </div>
                <div className="mt-3">
                  <h6><strong>Tags </strong>{""} <small className="text-muted">[optional]</small></h6>
                  <select
                    onChange={(e)=>handle(e)}
                    id="tags"
                    value={data.tags}
                    className="form-select"
                    aria-label="Default select example"
                  >
                    <option>Add Tag</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </select>
                </div>
                <div className="mt-2 d-flex justify-content-between">
                  <button
                    className="btn btn-outline-primary mt-3"
                    onClick={() => {
                      navigate("/assesment/pending");
                    }}
                  >
                    Previous Page
                  </button>
                  <button type="submit" className="btn btn-primary mt-3">
                    Save Assessment
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div className="col-md-4 p-3" style={{backgroundColor : "#E4E4FC"}}>
          <div className="">
            <h5 className="mt-3"><strong>Quick Tips</strong></h5>
            <p>
              Choose an Assessment Codename from the available options. Every
              assessment must have a unique codename for identification, Web
              applications use bug-related names; Host assessments use
              weather-related names.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Form;
