import React from "react";
import { Link } from "react-router-dom";
import PendingSearch from "../components/PendingSearch";

const Pending = () => {
  return (
    <div className="py-md-3">
      <div className="d-flex justify-content-between align-items-center">
        <div>
          <h3>Pending</h3>
          <p>
            All assesment and tests that are in creation process either being
            drafted or in the approval cycle
          </p>
        </div>
        <div>
          <Link to='/assesment/pending/form'><button className="btn btn-primary">Create Assessment</button></Link>
        </div>
      </div>
      <div>
        <PendingSearch></PendingSearch>
      </div>
    </div>
  );
};

export default Pending;
