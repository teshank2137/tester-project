import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import React from "react";
import Navbar from "./components/Navbar";
import Home from "./Pages/Home";
import Assesment from "./Pages/Assesment";
import Vulnerability from "./Pages/Vulnerability";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Approved from "./Pages/Approved";
import Pending from "./Pages/Pending";
import Form from "./Pages/Form";
import Exploitable from "./Pages/Exploitable";
import Suspected from "./Pages/Suspected";

function App() {
  return (
    <Router>
      <Navbar></Navbar>
      <Routes>
        <Route path="/" element={ <Home/> }/>
        <Route path="/vulnerability" element={ <Vulnerability/> }>
          <Route path="/vulnerability/exploitable" element={ <Exploitable/>}></Route>
          <Route path="/vulnerability/suspected" element={ <Suspected/>}></Route>
        </Route>
        <Route path="/assesment" element={ <Assesment/> }>
          <Route path="/assesment/approved" element={<Approved/>}/>
          <Route path="/assesment/pending" element={<Pending/>}></Route>
          <Route path="/assesment/pending/form" element={<Form/>}></Route>
        </Route>
      </Routes>
    </Router>
  );
}

export default App;
